<?php

namespace Drupal\entity_media_usage\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for entity_media_usage settings.
 */
class EntityMediaUsageSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_media_usage_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_media_usage.settings'];
  }

  /**
   * Constructs a new setting form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_media_usage.settings');

    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_types = array_filter($entity_types, function (EntityTypeInterface $entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });

    // Active on.
    $active_on_default_value   = $config->get('active_on') ?? (in_array('node', array_keys($entity_types)) ? ['node' => 'node'] : []);
    $form['active_on_details'] = [
      '#type'        => 'details',
      '#open'        => TRUE,
      '#title'       => $this->t('Tab display'),
      '#description' => '<p>' . $this->t('Activate "Media Usage" tab on specific entity types.') . '</p>' .
      '<p>' . $this->t('Selected entity types will have a "Media Usage" tab in their local tasks.') . '</p>' .
      '<p>' . $this->t('Currently selected:&nbsp;<strong>@list</strong>', [
        '@list' => implode(', ', array_filter($active_on_default_value)),
      ]) . '</p>',
    ];
    $form['active_on_details']['active_on'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Content entity types'),
      '#options'       => array_combine(array_keys($entity_types), array_keys($entity_types)),
      '#default_value' => $active_on_default_value,
    ];

    // Entity type IDs.
    $entity_type_ids_default_value   = $config->get('entity_type_ids') ?? ['media' => 'media'];
    $form['entity_type_ids_details'] = [
      '#type'        => 'details',
      '#open'        => TRUE,
      '#title'       => $this->t('Referenced entity types display'),
      '#description' => '<p>' . $this->t('Select which entity types to displayed in the "Media Usage" tab.') . '</p>' .
      '<p>' . $this->t('Selected entity types will be shown in list.') . '</p>' .
      '<p>' . $this->t('Currently selected:&nbsp;<strong>@list</strong>', [
        '@list' => implode(', ', array_filter($entity_type_ids_default_value)),
      ]) . '</p>',
    ];
    $form['entity_type_ids_details']['entity_type_ids'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Content entity types'),
      '#options'       => array_combine(array_keys($entity_types), array_keys($entity_types)),
      '#default_value' => $entity_type_ids_default_value,
    ];

    // Views.
    $views = $this->entityTypeManager->getStorage('view')->loadMultiple();

    $form['media_usage_view'] = [
      '#type'        => 'details',
      '#open'        => TRUE,
      '#title'       => $this->t('View'),
      '#description' => '<p>' . $this->t('Select a custom View to display referenced entities in Media Usage.') . '</p>',
    ];
    foreach ($entity_types as $entity_type) {
      $form['media_usage_view'][$entity_type->id() . '_media_usage_view'] = [
        '#type'          => 'select',
        '#empty_option'  => $this->t('Custom table (default)'),
        '#options'       => array_combine(array_keys($views), array_keys($views)),
        '#title'         => $entity_type->getSingularLabel(),
        '#default_value' => $config->get($entity_type->id() . '_media_usage_view'),
        '#states'        => [
          'visible' => [
            'input[name="entity_type_ids[' . $entity_type->id() . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    $form['#attached']['library'][] = 'entity_media_usage/form';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_media_usage.settings');

    $active_on_diff = array_diff(
      array_filter($config->get('active_on') ?? []),
      array_filter($form_state->getValue('active_on') ?? [])
    );

    $exclude = ['submit', 'form_build_id', 'form_token', 'form_id', 'op'];
    foreach ($form_state->getValues() as $key => $data) {
      if (!in_array($key, $exclude)) {
        $config->set($key, $data);
      }
    }
    $config->save();

    // Show/hide newly activated/disabled operation links.
    if (!empty($active_on_diff)) {
      $this->entityTypeManager->clearCachedDefinitions();
      $this->messenger()->addStatus($this->t('Entity type caches has been reset.'));
    }

    parent::submitForm($form, $form_state);
  }

}
