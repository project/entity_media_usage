<?php

namespace Drupal\entity_media_usage;

use Drupal\Core\Config\Config;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manipulates entity type information.
 */
class EntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * EntityTypeInfo constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Config\Config $config
   *   A config factory instance.
   */
  public function __construct(AccountInterface $current_user, Config $config) {
    $this->currentUser = $current_user;
    $this->config      = $config;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory')->get('entity_media_usage.settings')
    );
  }

  /**
   * Adds entity_media_usage links to appropriate entity types.
   *
   * This is an alter hook bridge.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    $entity_type_with_link_template = $this->config->get('active_on') ?? [];
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!in_array($entity_type_id, array_filter($entity_type_with_link_template))) {
        continue;
      }
      if (($entity_type->getFormClass('default') || $entity_type->getFormClass('edit')) && $entity_type->hasLinkTemplate('edit-form')) {
        $entity_type->setLinkTemplate('media_usage', "/$entity_type_id/{{$entity_type_id}}/media-usage");
      }
    }
  }

  /**
   * Adds Media Usage operations on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];
    if ($this->currentUser->hasPermission('access entity_media_usage information')) {
      if ($entity->hasLinkTemplate('media_usage')) {
        $operations['entity_media_usage'] = [
          'title'  => $this->t('Media usage'),
          'weight' => 100,
          'url'    => $entity->toUrl('media_usage'),
        ];
      }
    }
    return $operations;
  }

}
