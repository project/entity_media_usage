<?php

namespace Drupal\entity_media_usage\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Returns responses for entity_media_usage module routes.
 */
class EntityMediaUsageController extends ControllerBase {

  /**
   * Display the list of media for a given entity.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function list(RouteMatchInterface $route_match) {
    $current_entity = $this->getEntityFromRouteMatch($route_match);
    if (!$current_entity instanceof EntityInterface) {
      return [
        '#markup' => $this->t('Missing entity reference.'),
      ];
    }

    $config = $this->config('entity_media_usage.settings');

    $selected_entity_types = $config->get('entity_type_ids') ?? ['media'];
    $selected_entity_types = array_filter($selected_entity_types, function ($selected) {
      return $selected;
    });

    $build = [];
    foreach ($selected_entity_types as $entity_type_id) {
      $ids = $this->extractEntityIdsFromEntity($entity_type_id, $current_entity);
      if (empty($ids)) {
        continue;
      }

      if (count($selected_entity_types) == 1 && $view_id = $config->get($entity_type_id . '_media_usage_view')) {
        // Use custom Views if possible.
        $build[$entity_type_id] = views_embed_view($view_id, 'default', implode('+', $ids));
      } else {
        // Use basic table instead.
        $build[$entity_type_id] = $this->getTable($entity_type_id, $ids);
      }
    }

    return !empty($build) ? $build : [
      '#markup' => $this->t('No referenced entities.'),
    ];
  }

  /**
   * Retrieves entity from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object as determined from the passed-in route match.
   */
  protected function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    $parameter_name = $route_match->getRouteObject()->getOption('_media_usage_entity_type_id');
    return $route_match->getParameter($parameter_name);
  }

  /**
   * Get media IDs out of a given entity's fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A given entity.
   *
   * @return array
   *   The list of entity IDs found in fields.
   */
  protected function extractEntityIdsFromEntity(string $entity_type_id, EntityInterface $entity) {
    $ids = [];
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      switch ($field_definition->getType()) {
        case 'image':
          foreach ($entity->get($field_name)->referencedEntities() as $reference) {
            $ids[] = $reference->id();
          }
          break;
        case 'entity_reference':
          if (($field_definition->getSettings()['target_type'] ?? NULL) == $entity_type_id) {
            foreach ($entity->get($field_name)->referencedEntities() as $reference) {
              $ids[] = $reference->id();
            }
          }
          break;
        case 'entity_reference_revisions':
          foreach ($entity->get($field_name)->referencedEntities() as $paragraph) {
            // Recursively call.
            $ids = array_merge($ids, $this->extractEntityIdsFromEntity($entity_type_id, $paragraph));
          }
          break;
        case 'text':
        case 'text_long':
        case 'text_with_summary':
          $ids = array_merge($ids, $this->extractEntityIdsFromText($entity_type_id, $entity->get($field_name)->getString()));
          break;
      }
    }

    return $ids;
  }

  /**
   * Parse textfields to get <data-entity-XXX> attributes.
   *
   * @param string $entity_type_id
   *   A given entity type to look for.
   * @param string $text
   *   A given string, with HTML por favor.
   *
   * @return array
   *   The list of entity IDs found.
   *
   * @see \Drupal\media\Plugin\Filter\MediaEmbed::process();
   */
  protected function extractEntityIdsFromText(string $entity_type_id, string $text) {
    $ids   = [];
    $dom   = Html::load($text);
    $xpath = new \DOMXPath($dom);
    $query = $this->entityTypeManager()->getStorage($entity_type_id)->getQuery();

    if ($results = $xpath->query('//*[@data-entity-type="' . $entity_type_id . '" and normalize-space(@data-entity-uuid)!=""]')) {
      foreach ($results as $node) {
        $q    = clone $query;
        $uuid = $node->getAttribute('data-entity-uuid');
        $ids  = array_merge($ids, $q->condition('uuid', $uuid)->execute());
      }
    }

    return $ids;
  }

  /**
   * Prepare table render array to display entities.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param array $ids
   *   Given entity IDs.
   *
   * @return array
   *   The render array, obvisouly.
   */
  protected function getTable(string $entity_type_id, array $ids) {
    $manager      = $this->entityTypeManager();
    $entity_type  = $manager->getDefinition($entity_type_id);
    $storage      = $manager->getStorage($entity_type_id);
    $list_builder = $entity_type->hasListBuilderClass() ? $manager->getListBuilder($entity_type_id) : NULL;

    $rows = [];
    foreach ($storage->loadMultiple($ids) as $entity) {
      if ($entity_type_id == 'file') {
        $view_link = [
          '#type'  => 'link',
          '#url'   => Url::fromUri(file_create_url($entity->getFileUri()), [
            'attributes' => ['target' => '_blank'],
          ]),
          '#title' => $entity->label(),
        ];
        $actions = [];
      } else {
        $view_link = [
          '#type'               => 'link',
          '#url'                => $entity->hasLinkTemplate('canonical') ? $entity->toUrl('canonical') : Url::fromRoute('<nolink>'),
          '#title'              => $entity->label(),
          '#wrapper_attributes' => ['class' => ['label']],
        ];
        $edit_link = [
          '#type'               => 'link',
          '#url'                => $entity->hasLinkTemplate('edit-form') ? $entity->toUrl('edit-form') : Url::fromRoute('<nolink>'),
          '#title'              => $entity->label(),
          '#wrapper_attributes' => ['class' => ['label']],
        ];
        $actions = [
          '#type'  => 'link',
          '#url'   => $edit_link,
          '#title' => $this->t('Edit'),
        ];
        if ($list_builder) {
          $actions = [
            '#type'  => 'operations',
            '#links' => $list_builder->getOperations($entity),
          ];
        }
      }

      $row = [
        ['data' => $view_link],
        ['data' => $actions],
      ];
      $rows[] = $row;
    }

    $header = [
      $storage->getEntityType()->getPluralLabel(),
      ($entity_type_id == 'file') ? '' : $this->t('Links'),
    ];

    return [
      '#type'       => 'table',
      '#header'     => $header,
      '#rows'       => $rows,
      '#empty'      => $this->t('No @label found.', [
        '@label' => $entity_type->getPluralLabel(),
      ]),
      '#attributes' => [
        'class' => ['table-entity-usage', 'table-entity-usage--' . $entity_type_id],
      ],
      '#attached'   => [
        'library' => ['entity_media_usage/tables'],
      ],
    ];
  }
}
